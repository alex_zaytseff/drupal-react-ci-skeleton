#!/bin/bash

if [ "$1" = "local" ]
then
    echo "Envoronment - $1"
    # cp -R ops-tools/settings/local/* docroot/sites
    # chmod -R 777 docroot/sites/default/files
    # find docroot/.config -type d -name \* -exec chmod 775 {} \;
    # find docroot/.config -not -path '*/\.*' -type f -exec chmod 664 {} \;
    # echo "Setup permission for drupal files and config"
fi

file="./ops-tools/docker/docker-compose.$1.yml"

if [ -f "$file" ]
then
    # cp "$file" ./docker-compose.yml
    echo "Copy docker-compose file for $1 environment"
else
    echo "Config for '$1' env not found"
fi

if [ "$1" = "build" ]
then
    pattern="s/<version>/$2/g"
    if [ -z "$2" ]
    then
        echo "Set latest version to images"
        pattern="s/<version>/latest/g"
    else
        echo "Set images version to $2"
    fi

    # sed -i -- "$pattern" docker-compose.yml

    # cp -R ops-tools/settings/prod/* docroot/sites
    # echo "Copy drupal settings to default site"
fi

if [ "$1" != "build" ] && [ "$1" != "prepare" ]
then
    pattern="s/<server_port>/$2/g"

    if [ -z "$2" ]
    then
        echo "Set default port 8800"
        pattern="s/<server_port>/8800/g"
    else
        echo "Set port $2"
    fi

    sed -i -- "$pattern" docker-compose.yml
fi
