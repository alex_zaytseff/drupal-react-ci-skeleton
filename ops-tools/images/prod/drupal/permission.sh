#!/bin/sh
chgrp www-data -R /var/www/html/sites/default/files
chmod 775 -R /var/www/html/sites/default/files

chgrp www-data -R /var/www/html/.config/sync
find /var/www/html/.config -type d -name \* -exec chmod 775 {} \;
find /var/www/html/.config -not -path '*/\.*' -type f -exec chmod 664 {} \;

chmod 444 /var/www/html/sites/default/settings.php
