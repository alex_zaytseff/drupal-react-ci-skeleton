<?php

/**
 * @file
 * Implements Drush utility commands to set up Drupal projects.
 */

/**
 * Implements hook_drush_command().
 */
function build_drush_command() {
    $items = [];

    $items['build'] =[
        'description' => 'Run database updates, imports configuration, clears caches, etc.',
        'callback' => '_drush_build',
        'bootstrap' => DRUSH_BOOTSTRAP_NONE,
    ];

    $items['devify'] = [
        'description' => 'Configures the current database for development.',
        'callback' => '_drush_devify',
        'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
        'options' => [
            'enable-modules' => 'A comma separated list of modules to enable.',
            'uninstall-modules' => 'A comma separated list of modules to uninstall.',
            'update-config' => 'A comma separated list of configuration to update.',
        ],
        'examples' => [
            'drush devify' => 'Uses command default values to set up a database for development.',
            "drush devify --enable-modules=devel,kint --update-config='system.performance:css.preprocess:0,system.theme:admin:seven'" => 'Enables Devel and Kint modules and sets two configuration items.',
        ],
    ];

    return $items;
}

/**
 * Executes the build commands.
 */
function _drush_build() {
    $commands = array();

    $commands[] = array(
        'command' => 'state-set',
        'arguments' => ['system.maintenance_mode', 1],
        'options' => [],
    );
    $commands[] = array(
        'command' => 'updatedb',
        'arguments' => [],
        'options' => ['yes' => TRUE],
    );
    $commands[] = array(
        'command' => 'cache-rebuild',
        'options' => [],
    );
    $commands[] = array(
        'command' => 'config-import',
        'options' => ['yes' => TRUE],
    );
    $commands[] = array(
        'command' => 'state-set',
        'arguments' => ['system.maintenance_mode', 0],
        'options' => [],
    );
    $commands[] = array(
        'command' => 'cache-rebuild',
        'arguments' => [],
        'options' => [],
    );

    foreach ($commands as $command) {
        $command += array('alias' => '@self', 'arguments' => [], 'options' => []);
        drush_invoke_process($command['alias'], $command['command'], $command['arguments'], $command['options']);
        if (drush_get_error() != DRUSH_SUCCESS) {
            return drush_set_error('BUILD_FAILED', dt('Build failed on drush @command.', array('@command' => $command['command'])));
        }
    }

    drush_log(dt('Build completed.'), 'success');
}

/**
 * Callback for drush devify.
 */
function _drush_devify() {
    // Disable modules.
    $modules = drush_get_option('uninstall-modules', []);
    if (is_string($modules)) {
        $modules = explode(',', $modules);
    }
    \Drupal::service('module_installer')->uninstall($modules);

    // Enable modules.
    $modules = drush_get_option('enable-modules', []);
    if (is_string($modules)) {
        $modules = explode(',', $modules);
    }
    \Drupal::service('module_installer')->install($modules);

    // Update configuration.
    $options = drush_get_option('update-config', []);
    if (is_string($options)) {
        $options_raw = explode(',', $options);
        $options = [];
        foreach ($options_raw as $value) {
            $option = explode(':', $value);
            if (count($option) == 3) {
                $config_name = $option[0];
                $key = $option[1];
                $data = $option[2];
                $options[$config_name][$key] = $data;
            }
        }
    }
    foreach ($options as $config_name => $pairs) {
        foreach ($pairs as $key => $data) {
            \Drupal::service('config.factory')
                ->getEditable($config_name)
                ->set($key, $data)
                ->save();
        }
    }

    drush_log(dt('Devified!'), 'success');
}